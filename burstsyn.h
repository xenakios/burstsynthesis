#pragma once

#include <vector>
#include <array>
#include <memory>
#include <random>

#ifndef WIN32
namespace std
{
template<typename T, typename... Args>
inline std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(args)...);
}
}
#endif

template<typename T>
inline T bound_value(T minval, T val, T maxval)
{
	if (val < minval)
		return minval;
	if (val > maxval)
		return maxval;
	return val;
}

class randgen
{
public:
	randgen() {}
	randgen(unsigned long int seed) : a(seed), m_randgen(seed) {}
	unsigned long int a = 381411193;
	unsigned long int b = 1274403;
	unsigned long int c = 7034261;
	unsigned long int next()
	{
		return (*this)();
	}
	unsigned long int operator()()
	{
		if (m_freezed==true)
			return a;
		//std::uniform_int_distribution<unsigned long int> dist;
		//return dist(m_randgen);
		return a = a*b + c;
	}
	bool m_freezed = false;
	std::mt19937 m_randgen;
};
// TimeIntervals({32768,2743,1392,1334,1657,911,881,881,5 }),
class burstsyn
{
public:
	burstsyn(bool quad_output, unsigned long int seed,std::shared_ptr<randgen> rgen=nullptr) : 
		rnd(rgen), m_quad_output(quad_output),
		VoiceDistance(8), m_VoiceCurrentAmplitude(400), m_VoiceCurrentAmplitudeChangeRate(400),
		VolumeLimit({ 36000,54000 }),
		TimeIntervals({ 2552,2743,1392,1334,1657,911,881,2000 }),
		IndicatorsOfChange({ 0,0,0,0,0,0,0,0,0 })
	{
		m_len_in_bytes = 4 * 600 * 44100;
		OutputBuffer.resize(65536 * 4);
		if (rnd == nullptr)
		{
			rnd = std::make_shared<randgen>(seed);
		}
		for (int j = 0; j < VoiceDistance.size(); ++j)
		{
			VoiceDistance[j] = 1 + rnd->next() / 77 % 9;
		}
		m_VoiceCurrentAmplitudeChangeRate[0] = VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2) * rnd->next() / 77 % 9;
		for (int j = 1; j < m_ClusterDensity; ++j)
		{
			m_VoiceCurrentAmplitudeChangeRate[j] = m_VoiceCurrentAmplitudeChangeRate[j - 1] + VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2)*rnd->next() / 77 % 9;
		}
	}
	std::vector<short> OutputBuffer;
	void process_audio(int frames)
	{
		if (m_quad_output == true)
		{
			for (int i = 0; i < frames; ++i)
			{
				auto frame = next_frame();
				OutputBuffer[i * 4 + 0] = frame[0];
				OutputBuffer[i * 4 + 1] = frame[1];
				OutputBuffer[i * 4 + 2] = frame[2];
				OutputBuffer[i * 4 + 3] = frame[3];
			}
		}
		else
		{
			for (int i = 0; i < frames; ++i)
			{
				auto frame = next_frame();
				OutputBuffer[i * 2 + 0] = frame[0];
				OutputBuffer[i * 2 + 1] = frame[1];
			}
		}
	}
	std::array<int,4> next_frame()
	{
		if (m_byte_counter >= m_len_in_bytes)
			return{ 0,0,0,0 };
		m_ClusterDensity = std::min(m_ClusterDensity, 199);
		MaxDensity = std::max(MaxDensity, m_ClusterDensity);
		MinDensity = std::min(MinDensity, m_ClusterDensity);
		if (m_byte_counter % m_TimeBetweenChanges == 0)
		{
			if (m_byte_counter % (m_len_in_bytes / (m_part + 1)) == 0 || (m_byte_counter + 1) % (m_len_in_bytes / (m_part + 1)) == 0)
			{
				if (rnd->next() / 33 % 10 > 5)
				{
					m_VoiceJitter = 1;
				}
				else
				{
					m_VoiceJitter = 2 + rnd->next() / 443 % 5;

				}

				m_FloorPoint = !m_FloorPoint;
			}

			if (m_byte_counter > m_len_in_bytes*m_part / (m_part + 1))
			{

				m_TimeDensityDependency = m_TimeDensityDependency / 2;
				if (m_TimeDensityDependency == 0)
					m_TimeDensityDependency = 80;
				if (VolumeLimit[0] == 36000)
				{
					VolumeLimit[0] = 48000;
					VolumeLimit[1] = 60000;
				}
				else
				{
					VolumeLimit[0] = 36000;
					VolumeLimit[1] = 54000;
				}
				for (int k = 0; k < 8; ++k)
				{
					TimeIntervals[k] *= 2;
					if (TimeIntervals[k] <= 0)
						TimeIntervals[k] = 1234;
				}
				//++m_part;
				//if (Part<100)
				//	std::cout << (double)m_byte_counter/4.0/44100.0 << " " << Part << "\n";
			}
			//if (Part<100)
			//	std::cout << (double)m_byte_counter / 4.0 / 44100.0 << " ";
			for (int k = 0; k < 8; ++k)
			{
				if (m_manual_indicators_of_change == false)
					IndicatorsOfChange[k] = (m_ChangeCount % TimeIntervals[k]) == 0;
				//if (Part<100 && IndicatorsOfChange[k]!=0)
				//  std::cout << k << " *** " << IndicatorsOfChange[k] << " ";
			}
			//if (Part<100)
			//	std::cout << "\n";
			if (IndicatorsOfChange[0])
			{
				m_ClusterDensity = 50;
				m_VoiceCurrentAmplitudeChangeRate[0] = VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2)*rnd->next() / 77 % 9;
				for (int j = 1; j < m_ClusterDensity; ++j)
				{
					m_VoiceCurrentAmplitudeChangeRate[j] = m_VoiceCurrentAmplitudeChangeRate[j - 1] + VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2)*rnd->next() / 77 % 9;
				}
			}
			if (IndicatorsOfChange[1])
			{
				//std::cout << "********************************";
				m_ClusterDensity = m_part;
				m_VoiceCurrentAmplitudeChangeRate[0] = VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2)*rnd->next() / 77 % 9;
				LeftSpeaker = rnd->next() / 557 % 100;
				FrontSpeaker = rnd->next() / 557 % 100;
			}
			if (IndicatorsOfChange[2])
			{
				m_VoiceVolume = 100;
				m_JitteringVoicesCount = 1 + rnd->next() / 55 % 4;
				LeftSpeaker = rnd->next() / 557 % 100;
				FrontSpeaker = rnd->next() / 557 % 100;
			}
			if (IndicatorsOfChange[3])
			{
				m_VoiceVolume = VolumeLimit[1];
				LeftSpeaker = rnd->next() / 557 % 100;
				FrontSpeaker = rnd->next() / 557 % 100;
			}
			if (IndicatorsOfChange[4])
			{
				LeftSpeaker = rnd->next() / 557 % 100;
				FrontSpeaker = rnd->next() / 557 % 100;
				m_Jittering = rnd->next() / 99 % 4;
			}
			m_ClusterDensity = std::min(199, m_ClusterDensity);
			if (IndicatorsOfChange[5])
			{
				Shift = VoiceDistance[m_part % 7] + (m_part % 2) * rnd->next() / 551 % 100;
				for (int j = 0; j < m_ClusterDensity; ++j)
				{
					m_VoiceCurrentAmplitudeChangeRate[j] = Shift*m_VoiceCurrentAmplitudeChangeRate[j];
				}
			}
			if (IndicatorsOfChange[6])
			{
				Shift = VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2)*rnd->next() / 331 % 100;
				for (int j = 0; j < m_ClusterDensity; ++j)
				{
					m_VoiceCurrentAmplitudeChangeRate[j] = m_VoiceCurrentAmplitudeChangeRate[j] / Shift;
					if (m_VoiceCurrentAmplitudeChangeRate[j] <= 0)
					{
						m_VoiceCurrentAmplitudeChangeRate[j] = j;
					}
				}
				LeftSpeaker = rnd->next() / 217 % 100;
				FrontSpeaker = rnd->next() / 217 % 100;
			}
			if (IndicatorsOfChange[7])
			{
				Shift = VoiceDistance[m_part % VoiceDistance.size()] + (m_part % 2) * rnd->next() / 99 % 100;
				for (int j = 0; j < m_ClusterDensity; ++j)
				{
					m_VoiceCurrentAmplitudeChangeRate[j] = Shift*m_VoiceCurrentAmplitudeChangeRate[j];
				}
			}
			if (rnd->next() / 114 % 10>4)
			{
				--m_ClusterDensity;

			}
			else
			{
				++m_ClusterDensity;
			}
			if (m_ClusterDensity < m_part)
			{
				m_ClusterDensity = 8 + rnd->next() / 111 % 4;
				m_VoiceVolume = m_VoiceVolume * 1.2;
				if (m_VoiceVolume > VolumeLimit[0])
				{
					m_ClusterDensity = m_ClusterDensity / 2;
					m_VoiceVolume = m_VoiceVolume / 25;
					for (int j = 0; j < m_ClusterDensity; ++j)
					{
						m_VoiceCurrentAmplitudeChangeRate[j] /= (j + 1);
					}
				}
				LeftSpeaker = rnd->next() / 557 % 100;
				FrontSpeaker = rnd->next() / 557 % 100;
			}

			if (m_part*PartDensityDependency > 80)
			{
				--PartDensityDependency;
			}

			if (m_ClusterDensity > 80 - m_part*PartDensityDependency)
			{
				m_ClusterDensity /= 2;
				m_VoiceVolume = m_VoiceVolume / 1.3;
				if (m_VoiceVolume < 200)
				{
					m_ClusterDensity = m_ClusterDensity * 2;
					m_VoiceVolume = m_VoiceVolume * 3;
					for (int j = 0; j < m_ClusterDensity; ++j)
					{
						m_VoiceCurrentAmplitudeChangeRate[j] *= 2;
					}
				}
				LeftSpeaker = rnd->next() / 557 % 100;
				FrontSpeaker = rnd->next() / 557 % 100;
			}
			m_TimeBetweenChanges = m_ClusterDensity * m_TimeDensityDependency;
			for (int j = 0; j < m_JitteringVoicesCount; ++j)
			{
				switch (m_Jittering)
				{
				case 0: break;
				case 1: m_VoiceCurrentAmplitudeChangeRate[rnd->next() / 67 % m_ClusterDensity] += m_VoiceJitter;
					break;
				case 2:
					m_VoiceCurrentAmplitudeChangeRate[rnd->next() / 67 % m_ClusterDensity] -= m_VoiceJitter;
					break;
				case 3:
					int temp1 = rnd->next() / 67 % m_ClusterDensity;
					m_VoiceCurrentAmplitudeChangeRate[temp1] += m_VoiceJitter;
					temp1 = rnd->next() / 67 % m_ClusterDensity;
					m_VoiceCurrentAmplitudeChangeRate[temp1] -= m_VoiceJitter;
					break;
				}
				++m_ChangeCount;
			}
		}
		SampleValue = 0;
		for (int j = 0; j < m_ClusterDensity; ++j)
		{
			m_VoiceCurrentAmplitude[j] = m_VoiceCurrentAmplitude[j] + m_VoiceCurrentAmplitudeChangeRate[j];
			if (m_VoiceCurrentAmplitude[j] > m_VoiceVolume)
				m_VoiceCurrentAmplitude[j] = m_FloorPoint ? -m_VoiceVolume : 0;
			SampleValue += m_VoiceCurrentAmplitude[j];
		}
		RightSpeaker = 100 - LeftSpeaker;
		int BackSpeaker = 100 - FrontSpeaker;
		m_byte_counter += 2;
		m_byte_counter += 2;
		int factor;
		if (m_quad_output == true)
		{
			factor = 10000;
			return{ SampleValue*LeftSpeaker*FrontSpeaker / factor,
				SampleValue*RightSpeaker*FrontSpeaker / factor,
				SampleValue*LeftSpeaker*BackSpeaker / factor,
				SampleValue*RightSpeaker*BackSpeaker / factor };
		}
		factor = 100;
		return{ SampleValue * LeftSpeaker / factor,
				 SampleValue * RightSpeaker / factor,
				 0,0 };
		

	}
	int LeftSpeaker = 0;
	int MaxDensity = 0;
	int MinDensity = 100000;
	int m_part = 1;
	int m_TimeBetweenChanges = 882;
	int m_ChangeCount = 0;
	std::shared_ptr<randgen> rnd;
	std::vector<int> IndicatorsOfChange;
	bool m_manual_indicators_of_change = false;
private:
	
	int m_byte_counter = 0;
	
	
	int SampleValue = 0;
	
	int RightSpeaker = 0;
	int FrontSpeaker = 0;
	int m_ClusterDensity = 7;
	std::vector<int> m_VoiceCurrentAmplitude;
	std::vector<int> m_VoiceCurrentAmplitudeChangeRate;
	int m_VoiceVolume = 888;
	int m_FloorPoint = 0;
	int m_Jittering = 0;
	int m_JitteringVoicesCount = 2;
	int m_VoiceJitter = 1;
	int m_TimeDensityDependency = 40;
	std::vector<int> VolumeLimit;
	std::vector<int> TimeIntervals;
	
	std::vector<int> VoiceDistance;
	int Shift = 0;
	
	int PartDensityDependency = 5;
	bool m_quad_output = false;
	unsigned long int m_len_in_bytes = 42336000;
};
