#include <stdio.h>
#include <stdlib.h>
#include <vector>
//#include "conio.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include "burstsyn.h"
#include "sndfile.h"
#include "RtMidi.h"
#include <thread>
#include <map>
#include <FreeVerb.h>
#include <BiQuad.h>
#include "pad.h"
#ifdef WIN32
#include <ppl.h>


template<typename It, typename F>
inline void parallel_for_each(It a, It b, F&& f)
{
    Concurrency::parallel_for_each(a,b,std::forward<F>(f));
}
#undef min
#undef max
#else
#include "dispatch/dispatch.h"
template<typename It, typename F>
inline void parallel_for_each(It a, It b, F&& f)
{
    size_t count=std::distance(a,b);
    using data_t=std::pair<It,F>;
    data_t helper=data_t(a,std::forward<F>(f));
    dispatch_apply_f(count, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), &helper, [](void* ctx,size_t cnt)
                     {
                         data_t* d=static_cast<data_t*>(ctx);
                         auto elem_it=std::next(d->first,cnt);
                         (*d).second(*(elem_it));
                     });
}
#endif


void generate_burst()
{
	bool is_quad_output = false;
	int num_out_channels = 2;
	if (is_quad_output == true)
		num_out_channels = 4;
	SF_INFO sfoutinfo; memset(&sfoutinfo, 0, sizeof(SF_INFO));
	sfoutinfo.channels = num_out_channels;
	sfoutinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
	sfoutinfo.samplerate = 44100;
#ifdef WIN32
	SNDFILE* sfout = sf_open("C:/MusicAudio/Burst8.wav", SFM_WRITE, &sfoutinfo);
#else
    SNDFILE* sfout = sf_open("/Users/teemu/ReaperProjects/burstout/Burst02.wav", SFM_WRITE, &sfoutinfo);
#endif
	if (sfout != 0)
	{
		int bufsize = 4096;
		auto shared_rand_gen = std::make_shared<randgen>(36210);
		burstsyn bs(is_quad_output,0,shared_rand_gen);
		std::vector<short> diskoutbuf(bufsize * num_out_channels);
		std::vector<short> mixbuf(bufsize * num_out_channels);
		int samplecounter = 0;
		while (samplecounter < 180 * 44100)
		{
			bs.process_audio(bufsize);
			
			sf_writef_short(sfout, bs.OutputBuffer.data(), bufsize);
			samplecounter += bufsize;
		}
		sf_close(sfout);
		std::cout << "max density was " << bs.MaxDensity << "\n";
		std::cout << "finished\n";
	}
	else std::cout << "error opening soundfile\n";
}

void generate_multithreaded_burst()
{
	bool is_quad_output = true;
	int num_out_channels = 2;
	if (is_quad_output == true)
		num_out_channels = 4;
	SF_INFO sfoutinfo; memset(&sfoutinfo, 0, sizeof(SF_INFO));
	sfoutinfo.channels = num_out_channels;
	sfoutinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
	sfoutinfo.samplerate = 44100;
#ifdef WIN32
	SNDFILE* sfout = sf_open("C:/MusicAudio/Burst_MT_08.wav", SFM_WRITE, &sfoutinfo);
#else
    SNDFILE* sfout = sf_open("/Users/teemu/ReaperProjects/burstout/Burst_MT_02.wav", SFM_WRITE, &sfoutinfo);
#endif
	if (sfout != 0)
	{
		int bufsize = 32768;
		auto shared_rand = std::make_shared<randgen>(1517);
		std::vector<burstsyn> synths{ burstsyn(is_quad_output, 0, shared_rand),
									  burstsyn(is_quad_output, 0, shared_rand) };
		std::vector<short> diskoutbuf(bufsize * num_out_channels);
		std::vector<short> mixbuf(bufsize * num_out_channels);
		int samplecounter = 0;
		while (samplecounter < 180 * 44100)
		{
			std::fill(mixbuf.begin(), mixbuf.end(), 0);
			// process synth instances in different threads (hopefully...PPL or GCD might decide it won't use multiple threads...)
			parallel_for_each(synths.begin(), synths.end(),
				[bufsize](burstsyn& e) { e.process_audio(bufsize); });
			// threads finished, now mix
			for (auto& syn : synths)
			{
				
				for (int i = 0; i < bufsize*num_out_channels; ++i)
					mixbuf[i] += syn.OutputBuffer[i] / 2;
			}
			sf_writef_short(sfout, mixbuf.data(), bufsize);
			samplecounter += bufsize;
		}
		sf_close(sfout);
		//std::cout << "max density was " << bs.MaxDensity << "\n";
		std::cout << "finished\n";
	}
	else std::cout << "error opening soundfile\n";
}

void generate_multiple_bursts()
{
	bool is_quad_output = true;
	int num_out_channels = 2;
	if (is_quad_output == true)
		num_out_channels = 4;
	int num_out_files = 16;
	for (int i = 0; i < num_out_files; ++i)
	{

		SF_INFO sfoutinfo; memset(&sfoutinfo, 0, sizeof(SF_INFO));
		sfoutinfo.channels = num_out_channels;
		sfoutinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
		sfoutinfo.samplerate = 44100;
		std::string outfn = "C:/MusicAudio/BurstGenerated/Burst_" + std::to_string(i) + ".wav";
		SNDFILE* sfout = sf_open(outfn.c_str(), SFM_WRITE, &sfoutinfo);
		if (sfout != 0)
		{
			int bufsize = 4096;
			burstsyn bs(is_quad_output, 1520+i);
			std::vector<short> buf(bufsize * num_out_channels);
			int samplecounter = 0;
			while (samplecounter < 240 * 44100)
			{
				bs.process_audio(bufsize);
				sf_writef_short(sfout, bs.OutputBuffer.data(), bufsize);
				samplecounter += bufsize;
			}
			sf_close(sfout);
			std::cout << "max density was " << bs.MaxDensity << "\n";
			std::cout << i << " finished\n";
		}
		else std::cout << "error opening soundfile\n";
	}
}

using namespace PAD;

class ErrorLogger : public DeviceErrorDelegate {
public:
	void Catch(SoftError e) { std::cerr << "*Soft " << e.GetCode() << "* :" << e.what() << "\n"; }
	void Catch(HardError e) { std::cerr << "*Hard " << e.GetCode() << "* :" << e.what() << "\n"; }
};

struct syninfo
{
	syninfo() {}
	burstsyn* syn = nullptr;
	double* volume = nullptr;
	double* verbmix = nullptr;
};

void my_in_callback(double timeStamp, std::vector<unsigned char> *message, void *userData)
{
	if (message->size() != 3)
		return;
	syninfo* syn = (syninfo*)userData;
	unsigned char b0 = (*message)[0];
	unsigned char b1 = (*message)[1];
	unsigned char b2 = (*message)[2];
	if (b0==176)
	{
		//std::cout << (int)b1 << " ";
		if (b1 == 97)
		{
			*syn->volume = 2.0 / 127 * b2;
		}
		if (b1 == 98)
		{
			syn->syn->m_part = 1 + (64.0 / 127 * b2);
		}
		if (b1 == 99)
		{
			syn->syn->rnd->a = 4294967295 / 127 * b2;
		}
		if (b1 == 100)
		{
			*syn->verbmix = 1.0 / 127 * b2;
		}
		if (b1 == 65)
		{
			if (b2 < 64)
			 	syn->syn->rnd->m_freezed = false;
			else syn->syn->rnd->m_freezed = true;
		}
		if (b1 == 67)
		{
			if (b2 < 64)
				syn->syn->m_manual_indicators_of_change = false;
			else syn->syn->m_manual_indicators_of_change = true;
		}
		if (b1 >= 73 && b1 < 81)
		{
			int index = b1 - 73;
			if (b2 < 64)
				syn->syn->IndicatorsOfChange[index] = 0;
			else syn->syn->IndicatorsOfChange[index] = 1;
		}
	}
}

void play_realtime()
{
	std::cout << "Hello from PAD " << PAD::VersionString() << "!" << std::endl;
    ErrorLogger erlog;
    Session myAudioSession(true, &erlog);
	for (auto& dev : myAudioSession)
	{
		std::cout << dev << "\n  * Stereo : " << dev.DefaultStereo()
			<< "\n  * All    : " << dev.DefaultAllChannels() << "\n\n";
	}
	auto audioDevice = myAudioSession.FindDevice("Built-in Output");
	if (audioDevice != myAudioSession.end())
	{
		auto midi_input = std::make_unique<RtMidiIn>();
		if (midi_input == nullptr)
		{
			std::cout << "could not open midi input\n";
			//return;
		}
		
		
		//std::this_thread::sleep_for(std::chrono::milliseconds(10));
		
		std::mt19937 gen;
		std::uniform_real_distribution<float> dist(-0.2f, 0.2f);
		int seed = 666;
		burstsyn syn(false, seed);
		int num_minuses = 0;
		int num_pluses = 0;
		double volume = 1.0;
		int samplecounter = 0;
		syninfo info;
		info.syn = &syn;
		info.volume = &volume;
		if (midi_input!=nullptr)
        {
            midi_input->setCallback(my_in_callback, (void*)&info);
            midi_input->openPort(1);
        }
		stk::FreeVerb verb;
		int heartbeatcounter = 0;
		double verbmix = 0.5;
		info.verbmix = &verbmix;
		audioDevice->BufferSwitch = [&heartbeatcounter, &verbmix, &verb,&samplecounter,&volume, &num_minuses,&num_pluses, &syn,&dist,&gen](PAD::IO io)
		{
			syn.process_audio(io.numFrames);
			const double gain = 1.0 / 65536 / 4;
			for (int i = 0; i < io.numFrames; ++i)
			{
				double synout_0 = syn.OutputBuffer[i * 2 + 0]*gain*volume;
				double synout_1 = syn.OutputBuffer[i * 2 + 1]*gain*volume;
				double verbout_0 = verb.tick(synout_0, synout_1, 0);
				double verbout_1 = verb.tick(synout_0, synout_1, 1);
				io.output[i * 2 + 0] = verbout_0*verbmix+synout_0*(1.0-verbmix);
				io.output[i * 2 + 1] = verbout_1*verbmix+synout_1*(1.0-verbmix);
				
			}
			samplecounter += io.numFrames;
			heartbeatcounter += io.numFrames;
			if (heartbeatcounter >= 10 * 44100)
			{
				heartbeatcounter = 0;
				std::cout << (double)samplecounter / 44100 << " seconds elapsed\n";
			}
		};
		audioDevice->Open(audioDevice->DefaultStereo());
		while (true)
		{
			int ch = getchar();
			if (ch == 'q') break;
			if (ch == 'w') volume += 0.1;
			if (ch == 's') volume -= 0.1;
			if (ch == 'e') syn.m_part = bound_value(1, syn.m_part + 1, 900);
			if (ch == 'd') syn.m_part = bound_value(1, syn.m_part - 1, 900);
			if (ch == 'l') ++syn.m_ChangeCount;
			if (ch == 'p') syn.m_TimeBetweenChanges = 4;
			if (ch == 'i') syn.rnd->a += 1023;
			if (ch == 'k') syn.rnd->a -= 1023;
			if (ch == 'f') syn.rnd->m_freezed = !syn.rnd->m_freezed;
			if (ch >= '1' && ch <= '8')
			{
				int index = ch - '1';
				if (syn.IndicatorsOfChange[index]==0)
					syn.IndicatorsOfChange[index] = 1;
				else
					syn.IndicatorsOfChange[index] = 0;
			}
			std::cout << "vol " << volume << " part " << syn.m_part << " time between " << syn.m_TimeBetweenChanges << " ";
			std::cout << "seed " << syn.rnd->a << " ";
			for (int i = 0; i < 8; ++i)
				std::cout << syn.IndicatorsOfChange[i] << " ";
			std::cout << "\n";
		};
		audioDevice->Close();
		std::cout << "finished playing\n";
		std::cout << num_minuses << " " << num_pluses << "\n";
	}
	else std::cout << "could not open device\n";
}



void test_midi()
{
	auto midi_input=std::make_unique<RtMidiIn>();
	if (midi_input == nullptr)
		return;
	unsigned int nPorts = midi_input->getPortCount();
	std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";

	for (unsigned i = 0; i<nPorts; i++) {
		std::string portName = midi_input->getPortName(i);
		std::cout << "  Input Port #" << i + 1 << ": " << portName << '\n';
	}
	midi_input->setCallback(my_in_callback, nullptr);
	midi_input->openPort(1);
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

int main(int argc, char** argv)
{
	//test_midi();
	//generate_multithreaded_burst();
	//generate_multiple_bursts();
	//generate_burst();
	play_realtime();
	return 0;
}
